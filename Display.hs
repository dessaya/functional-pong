module Display (display, reshape)
where

import Graphics.Rendering.OpenGL hiding (Point)
import Graphics.UI.GLUT hiding (Point)
import Pong
import Data.IORef

quad w h = do 
  renderPrimitive Quads $ do
    vertex $ Vertex2 (0::GLfloat) (0::GLfloat)
    vertex $ Vertex2 w (0::GLfloat)
    vertex $ Vertex2 w h
    vertex $ Vertex2 (0::GLfloat) h

pad (Point x y) = preservingMatrix $ do
	translate $ Vector3 x y 0
	translate $ Vector3 (-padWidth / 2) (-padHeight / 2) 0
	quad padWidth padHeight

midline = preservingMatrix $ do
	let w = (0.01::GLfloat)
	translate $ Vector3 (-w / 2) (-1) 0
	quad w 2

ball (Point x y) = preservingMatrix $ do
	translate $ Vector3 x y 0
	translate $ Vector3 (-ballWidth / 2) (-ballWidth / 2) 0
	quad ballWidth ballWidth

display state = do
	clear [ ColorBuffer ]
	s <- get state
	
	color $ Color3 (0.5::GLfloat) (0.5::GLfloat) (0.5::GLfloat)
	midline

	color $ Color3 (1::GLfloat) (1::GLfloat) (1::GLfloat)
	pad $ padPos Pad1 s
	pad $ padPos Pad2 s
	ball $ ballPos $ ballState s
	
	flush

reshape s@(Size w h) = do
	viewport $= (Position 0 0, s)

