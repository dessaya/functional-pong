import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Display
import Pong
import Data.IORef

setTimeout state keyStates = addTimerCallback (floor tickMS) (updateState state keyStates)

updateState state keyStates = do
	ks <- get keyStates
	modifyIORef state (updatePong ks)
	postRedisplay Nothing
	setTimeout state keyStates

keyboardMouse :: (IORef KeyStates) -> Key -> KeyState -> Modifiers -> Position -> IO ()
keyboardMouse keyStates (SpecialKey KeyUp) status _ _ = do
	ks <- get keyStates
	keyStates $= ks { upPressed = status == Down }
keyboardMouse keyStates (SpecialKey KeyDown) status _ _ = do
	ks <- get keyStates
	keyStates $= ks { downPressed = status == Down }
keyboardMouse keyStates (Char 'q') status _ _ = do
	ks <- get keyStates
	keyStates $= ks { qPressed = status == Down }
keyboardMouse keyStates (Char 'a') status _ _ = do
	ks <- get keyStates
	keyStates $= ks { aPressed = status == Down }
keyboardMouse _ _ _ _ _ = return ()

main = do
	(progname, _) <- getArgsAndInitialize
	initialWindowSize $= (Size 500 500)
	createWindow "Pong!"
  
	state <- newIORef initPong
	keyStates <- newIORef initKeyStates
  
	displayCallback $= (display state)
	keyboardMouseCallback $= Just (keyboardMouse keyStates)
	reshapeCallback $= Just reshape
	setTimeout state keyStates
  
	mainLoop

